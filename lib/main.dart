import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Layout demo',
      home: AppNhat(),
      debugShowCheckedModeBanner: false,  // bỏ baner Debug
    );
  }
}

class AppNhat extends StatefulWidget {
  @override
  _AppNhatState createState() => _AppNhatState();
}

class _AppNhatState extends State<AppNhat> {
  
  int _counter = 0;
  int _counter2 = 0;
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: const Text('Flutter Layout demo'), // Tiêu đề thanh appBar
        ),
        backgroundColor: Colors.blue,
      ),

      body: ListView(
        children: [

          Image.asset('assets/images/anh.jpg'), // link ảnh
         
          Container(
            padding: const EdgeInsets.all(30), // khung chữ cách xung quanh 30
            child: Row(
              children: [
                Expanded( // tự động căn chỉnh cho đầy không gian của hàng
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start, // Khung lề start
                    children: [
                      Text(
                        'Oeschinen Lake Campground', 
                        style: TextStyle(
                          fontWeight: FontWeight.bold // Chữ đậm
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(top: 7), // cách dòng trên 10
                        child: Text(
                          'Kandersteg, Switzerland',
                          style: TextStyle(
                            color: Colors.black38,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                IconButton(
                  icon: const Icon(Icons.star, color: Colors.red),
                  onPressed: () {
                    setState(() {
                      _counter ++;
                    });
                  },),
                Text('$_counter'),
              ],
            ),
          ),

          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly, // Tự động cách nhau một khoản đồng đều
              children: [
                Container( // Container bọc CALL
                  child: Column(
                    children: [
                      Icon(Icons.call, color: Colors.blue),
                      Container(
                        margin: const EdgeInsets.only(top: 8),
                        child: (
                          Text('CALL', style: TextStyle(color: Colors.blue))
                        ),
                      )
                    ],
                  ),
                ),
                Container( // Container bọc ROUTE
                  child: Column(
                    children: [
                      Icon(Icons.near_me, color: Colors.blue),
                      Container(
                        margin: const EdgeInsets.only(top: 8),
                        child: (
                          Text('ROUTE', style: TextStyle(color: Colors.blue))
                        ),
                      )
                    ],
                  ),
                ),
                Container( // Container bọc SHARE
                  child: Column(
                    children: [
                      Icon(Icons.share, color: Colors.blue),
                      Container(
                        margin: const EdgeInsets.only(top: 8),
                        child: (
                          Text('SHARE', style: TextStyle(color: Colors.blue))
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),


          Container(
            padding: const EdgeInsets.all(30),
            child: Text(
              "Oeschinen Lake (German: Oeschinensee) is a lake in the Bernese Oberland, Switzerland, 4 kilometres (2.5 mi) east of Kandersteg in the Oeschinen valley. At an elevation of 1,578 metres (5,177 ft), it has a surface area of 1.1147 square kilometres (0.4304 sq mi). Its maximum depth is 56 metres (184 ft)."
            ),
          ),

          Image.asset('assets/images/anh.jpg'), // link ảnh
         
          Container(
            padding: const EdgeInsets.all(30), // khung chữ cách xung quanh 30
            child: Row(
              children: [
                Expanded( // tự động căn chỉnh cho đầy không gian của hàng
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start, // Khung lề start
                    children: [
                      Text(
                        'Oeschinen Lake Campground', 
                        style: TextStyle(
                          fontWeight: FontWeight.bold // Chữ đậm
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(top: 7), // cách dòng trên 10
                        child: Text(
                          'Kandersteg, Switzerland',
                          style: TextStyle(
                            color: Colors.black38,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                IconButton(
                  icon: const Icon(Icons.star, color: Colors.red),
                  onPressed: () {
                    setState(() {
                      _counter2 ++;
                    });
                  },),
                Text('$_counter2'),
              ],
            ),
          ),


          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly, // Tự động cách nhau một khoản đồng đều
              children: [
                Container( // Container bọc CALL
                  child: Column(
                    children: [
                      Icon(Icons.call, color: Colors.blue),
                      Container(
                        margin: const EdgeInsets.only(top: 8),
                        child: (
                          Text('CALL', style: TextStyle(color: Colors.blue))
                        ),
                      )
                    ],
                  ),
                ),
                Container( // Container bọc ROUTE
                  child: Column(
                    children: [
                      Icon(Icons.near_me, color: Colors.blue),
                      Container(
                        margin: const EdgeInsets.only(top: 8),
                        child: (
                          Text('ROUTE', style: TextStyle(color: Colors.blue))
                        ),
                      )
                    ],
                  ),
                ),
                Container( // Container bọc SHARE
                  child: Column(
                    children: [
                      Icon(Icons.share, color: Colors.blue),
                      Container(
                        margin: const EdgeInsets.only(top: 8),
                        child: (
                          Text('SHARE', style: TextStyle(color: Colors.blue))
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),

          Container(
            padding: const EdgeInsets.all(30),
            child: Text(
              "Oeschinen Lake (German: Oeschinensee) is a lake in the Bernese Oberland, Switzerland, 4 kilometres (2.5 mi) east of Kandersteg in the Oeschinen valley. At an elevation of 1,578 metres (5,177 ft), it has a surface area of 1.1147 square kilometres (0.4304 sq mi). Its maximum depth is 56 metres (184 ft)."
            ),
          ),
        ],
      )
    );
  }
}
